# cdecrypt-docker
This repository lists everything needed to build and run Docker images for cdecrypt

## Build cdecrypt Image
Run the following in the cdecrypt-build directory:

```bash
docker build -t drkstrinc/cdecrypt-build:latest .
```

## Create cdecrypt Container which will decrypt game files
Note: You can place multiple game directories in the games dir and cdecrypt will decrypt them all, one by one

Run the following command:

```bash
docker run --rm \
    -v /share/Container/cdecrypt/games:/games \
    drkstrinc/cdecrypt-build:latest
```
