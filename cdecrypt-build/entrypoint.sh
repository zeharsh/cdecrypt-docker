#!/bin/bash

#Find encrypted game dirs and call cdecrypt for each of them
for f in /games/*; do
    if [ -d "$f" ]; then
        /cdecrypt/cdecrypt $f
    fi
done

exec "$@"
